const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = merge(common, {
    mode: 'production',
    devtool: 'source-map',
    plugins: [
        new HtmlWebpackPlugin({
        favicon: "./src/favicon.ico",    
        template: './src/index.tmp.html',        
        title: 'sensor viewer',  
        // inject: true // Inject all scripts into the body 
        minify: false, // should html file be minified?
        inject: false,     
    }),
    new MiniCssExtractPlugin({ // Make sure MiniCssExtractPlugin instance is included in array before the PurifyCSSPlugin
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        // filename: '[name].css',
        // chunkFilename: '[id].css',
        filename: '[name].css',
        chunkFilename: '[chunkhash].css',
    }),
],
});