require('dotenv').config();
const path = require('path');
const webpack = require('webpack'); //e.g. for iusing DefinePlugin
// var helpers = require('./helpers');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

/**
 * flag Used to check if the environment is production or not
 */
const isProduction = (process.env.NODE_ENV === 'production');
//  const devMode = (process.env.NODE_ENV !== 'production');

/**
* Include hash to filenames for cache busting - only at production
*/
const fileNamePrefix = isProduction ? '[chunkhash].' : '';

module.exports = {
	// mode: process.env.NODE_ENV,
	context: __dirname,
	// entry: './src/main.ts',
	entry: {
		// vendor: './src/vendor.ts',
		polyfills: './src/polyfills.ts',
		main: './src/main.ts',
		// styles: "./src/app/styles.css"
	},
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: '[name].' + fileNamePrefix + 'js',
		//filename: fileNamePrefix + '[name].js',  // css loader will process all @import and url() with help of require()
		// publicPath: '/dist/',
		// sourceMapFilename: "[name].js.map"
	},

	// resolve: {
	//     alias: {
	//         "./images/layers.png$": path.resolve(
	//             __dirname,
	//             "./node_modules/leaflet/dist/images/layers.png"
	//         ),
	//         "./images/layers-2x.png$": path.resolve(
	//             __dirname,
	//             "./node_modules/leaflet/dist/images/layers-2x.png"
	//         ),
	//         "./images/marker-icon.png$": path.resolve(
	//             __dirname,
	//             "./node_modules/leaflet/dist/images/marker-icon.png"
	//         ),
	//         "./images/marker-icon-2x.png$": path.resolve(
	//             __dirname,
	//             "./node_modules/leaflet/dist/images/marker-icon-2x.png"
	//         ),
	//         "./images/marker-shadow.png$": path.resolve(
	//             __dirname,
	//             "./node_modules/leaflet/dist/images/marker-shadow.png"
	//         )
	//     }
	// },
	module: {
		rules: [
			{
				test: /\.(png|svg|jpg|gif)$/,
				use: [{
					loader: 'url-loader',
					options: {
						name: '[name].[ext]',
						outputPath: './assets/'
					}
				}]
			},

			{
				// test: /\.js$/,
				test: /\.(js|jsx|tsx|ts)$/,
				exclude: /(node_modules|bower_components)/,
				use:
					[
						{
							loader: 'babel-loader',
							// options: { configFileName: helpers.root('src', 'tsconfig.json') }
						},
						{
							loader: 'angular2-template-loader'
						}
					]

			},
			// {
			// 	test: /\.ts$/,
			// 	use: [{
			// 		loader: 'ts-loader',
			// 		options: { configFile: path.resolve(__dirname, 'tsconfig.json') }
			// 	}, 'angular2-template-loader'
			// 	]
			// },

			// {
			//     test: /\.html$/,                
			//     use: [
			//       {
			//         loader: 'raw-loader',
			//         // options: {
			//         //   minimize: true,                      
			//         // }
			//       }
			//     ],
			//     exclude: [helpers.root('src/index.html')]
			//   },
			{
				test: /\.html$/,
				// exclude: [/node_modules/],
				include: path.resolve(__dirname, 'src/app'),
				use: {
					loader: 'raw-loader',
					options: {
						esModule: false,
					},
				}
			},
			{
				test: /\.(css|scss)$/,
				// include: helpers.root('src', 'app'),
				include: path.resolve(__dirname, 'src/app'),
				use: {
					loader: 'raw-loader',
					options: {
						esModule: false,
					},
				}

				// use: ['style-loader', 'css-loader']
			},
			{
				test: /\.(scss|css)$/,
				// exclude: helpers.root('src', 'app'),
				exclude: path.resolve(__dirname, 'src/app'),
				use: [
					{
						loader: (isProduction === true) ? MiniCssExtractPlugin.loader : 'style-loader',
						// loader: 'style-loader',                      
						// loader: MiniCssExtractPlugin.loader,
						// options: {
						//     hmr: process.env.NODE_ENV === 'development',
						// },
					},
					// Translates CSS into CommonJS
					{
						loader: "css-loader",
						options: {
							sourceMap: true
						}
					},
					 // Compiles Sass to CSS
					 {
                        loader: "sass-loader",
                        options: {
                            sourceMap: true
                        }
                    }

				]
			},


		]
	},
	resolve: {
		extensions: ['*', '.js', '.jsx', '.tsx', '.ts'],
	},
	// devtool: 'inline-source-map',
	stats: {
		colors: true
	},

	optimization: {
		minimize: isProduction,
		minimizer: [
			new TerserPlugin({
				// cache: true,
				parallel: true,
				// sourceMap: true, // Must be set to true if using source-maps in production
				extractComments: true,
				terserOptions: {

					compress: {
						directives: false,
						// drop_console: true,
						// drop_debugger: true,
						// keep_classnames: false,
						// keep_fnames: false,
					},
					mangle: true, // Note `mangle.properties` is `false` by default.
					keep_classnames: false,
					keep_fnames: false,
				}
			})
		],
	},

	plugins: [
		// new MiniCssExtractPlugin({ // Make sure MiniCssExtractPlugin instance is included in array before the PurifyCSSPlugin
		//     // Options similar to the same options in webpackOptions.output
		//     // both options are optional
		//     // filename: '[name].css',
		//     // chunkFilename: '[id].css',
		//     filename: '[name].css',
		//     chunkFilename: '[chunkhash].css',
		// }),
	]
};


// https://medium.com/ag-grid/webpack-tutorial-understanding-ngtools-webpack-306dd7f9e07d