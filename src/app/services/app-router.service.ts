import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppRouterService {

  private router: Router

  constructor(router: Router) { 
    this.router = router;
  }

  public toDiagram() {
    this.router.navigate(['diagram']);
  }
  
}
