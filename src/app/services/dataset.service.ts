import { Injectable } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { Timespan } from '../../shared/models/timespan';
import * as moment from "moment";

const TIMESERIES_OPTIONS_CACHE_PARAM = 'timeseriesOptions';
const TIMESERIES_IDS_CACHE_PARAM = 'timeseriesIds';
const TIME_CACHE_PARAM = 'timeseriesTime';
// https://github.com/52North/helgoland-toolbox/blob/fe6af1b9df0e5d78eeec236e4690aeb7dc92119b/apps/helgoland/src/app/services/timeseries-service.service.ts#L22

import { TimeService } from '../../common/core/time/time.service';
import { DatasetOptions } from '../../shared/models/options';

@Injectable()
export class DatasetService<T extends DatasetOptions> {

    public datasetIds: string[] = [];
    public datasetOptions: Map<string, T> = new Map();

    private _timespan: Timespan;
    
    public datasetIdsChanged: EventEmitter<string[]> = new EventEmitter();

    constructor(private timeService: TimeService) {		
        this.initTimespan();
	}


    public get timespan(): Timespan {
        return this._timespan;
      }
    
      public set timespan(v: Timespan) {
        this._timespan = v;
        // this.timeSrvc.saveTimespan(TIME_CACHE_PARAM, this._timespan);
      }

      
    /**
      * Adds the dataset to the selection
      *
      * @param internalId
      * @param [options]
      * @returns Successfull added the dataset.
      */
    public addDataset(internalId: string, options?: T): boolean {
        if (this.datasetIds.indexOf(internalId) < 0) {
            this.datasetIds.push(internalId);
            // if (options) {
            //     this.datasetOptions.set(internalId, options);
            // } else {
            //     this.datasetOptions.set(internalId, this.createStyles(internalId));
            // }
            // this.saveState();
        } 
        // else if (options instanceof Array) {
        //     const temp = (this.datasetOptions.get(internalId) as DatasetOptions[]);
        //     options.forEach((e) => temp.push(e));
        //     // this.saveState();
        // }
        options.visible = true;
        this.datasetOptions.set(internalId, options);
        if(this.datasetOptions.size == 1){
        
            // this._timespan = new Timespan(options.firstValue.timestamp, options.lastValue.timestamp);
            this._timespan = this.timeService.centerTimespan(this._timespan, new Date(options.firstValue.timestamp));
        }

        this.datasetIdsChanged.emit(this.datasetIds);
        return true;
    }

    public removeAllDatasets() {
        this.datasetIds.length = 0;
        this.datasetOptions.clear();
        this.datasetIdsChanged.emit(this.datasetIds);
        // this.saveState();
    }

    public removeDataset(internalId: string) {
        const datasetIdx = this.datasetIds.indexOf(internalId);
        if (datasetIdx > -1) {
            this.datasetIds.splice(datasetIdx, 1);
            this.datasetOptions.delete(internalId);
        }
        this.datasetIdsChanged.emit(this.datasetIds);
        // this.saveState();
    }

    public hasDatasets(): boolean {
        return this.datasetIds.length > 0;
    }

    public hasDataset(id: string): boolean {
        // return this.datasetIds.indexOf(id) >= 0;
        return this.datasetOptions.has(id);
    }

    public updateDatasetOptions(options: T, internalId: string) {
        this.datasetOptions.set(internalId, options);
        // this.saveState();
    }

    private initTimespan() {
        if (!this._timespan) {
        //   this._timespan =  new Timespan(1323239694000, 1323844494000);    
        // //this.timeService.createByDurationWithEnd(moment.duration(1, 'days'), new Date(2011, 9), 'day');
        const today = moment();
        const from_date = today.startOf('isoWeek').toDate();
        const to_date = today.endOf('isoWeek').toDate();
        this._timespan = new Timespan(from_date, to_date);
        }
      }
    
}