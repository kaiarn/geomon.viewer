import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { Map, circleMarker } from 'leaflet';

// @Injectable({
//     // declares that this service should be created
//   // by the root application injector.
//   providedIn: 'root'
// })
@Injectable()
export class MarkerService {

  private capitalsUrl: string = 'https://raw.githubusercontent.com/do-community/angular-leaflet-example/master/src/assets/data/usa-capitals.geojson';
 
  constructor(private http: HttpClient) {
    // this.http = http;
  }

  static scaledRadius(val: number, maxVal: number): number {
    return 20 * (val / maxVal);
  }

//   makeCapitalMarkers(map: L.map): void {
//     this.http.get(this.capitalsUrl).subscribe((res: any) => {
//       for (const c of res.features) {
//         const lon = c.geometry.coordinates[0];
//         const lat = c.geometry.coordinates[1];
//         const marker = new L.marker([lat, lon]);

//         marker.addTo(map);
//       }
//     });
//   }

  makeCapitalCircleMarkers(map: Map): void {
    this.http.get(this.capitalsUrl).subscribe((res: any) => {

      let maxPop = Math.max(...res.features.map((x: any) => x.properties.population), 0);

      for (const c of res.features) {
        const lon = c.geometry.coordinates[0];
        const lat = c.geometry.coordinates[1];
        // const circle = L.circleMarker([lat, lon]);
        let circle = circleMarker([lat, lon], {
          radius: MarkerService.scaledRadius(c.properties.population, maxPop)
        });
        circle.addTo(map);
      }
    });
  }

}
