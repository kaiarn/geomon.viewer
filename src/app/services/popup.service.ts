import { Injectable } from '@angular/core';

@Injectable()
export class PopupService {

	constructor() { }

	makeCapitalPopup(data: any): string {
		return `` +
			`<div>Name: ${data.label}</div>` +
			`<a href=${data.href}  target="_blank">Visit station api</a>`
	}
}
