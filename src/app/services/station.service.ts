import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Station } from '../../shared/models/station';
import { MessageService } from './message.service';
import { STATIONS } from './mock-stations';

@Injectable({
	providedIn: 'root'
})
export class StationService {
private messageService;

	constructor(messageService: MessageService) {
		this.messageService = messageService;
	}

	getStations(): Observable<Station[]> {
		const stations = of(STATIONS);
		this.messageService.add('StationService: fetched stations');
		return stations;
	}

	getStation(id: string): Observable<Station> {
		// For now, assume that a hero with the specified `id` always exists.
		// Error handling will be added in the next step of the tutorial.
		const station = STATIONS.find(h => h.id === id)!;
		this.messageService.add(`StationService: fetched station id=${id}`);
		return of(station);
	}
}
