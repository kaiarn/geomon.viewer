import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MapViewComponent } from "./views/map-view/map-view.component";

import { ComponentsModule } from '../../src/common/components/components.module';
import { GraphjsModule } from '../../src/common/graphjs/graphjs.module';
// import { CoreModule } from '../../src/common/core/core.module';

import { HttpClientModule, HttpClient } from '@angular/common/http'; //for http requests
import { MarkerService } from './services/marker.service';
import { PopupService } from './services/popup.service';
import { DatasetApiService } from "./services/dataset-api.service";
import { HttpService } from "./services/http.service";
import { StationService } from "./services/station.service";
import { MessagesComponent } from './messages/messages.component';
import { MessageService } from "./services/message.service";
import { DatasetService } from "./services/dataset.service";
import { MapService } from '../common/components/services/map.service';
import { DiagramViewComponent } from './views/diagram-view/diagram-view.component';
// import { MomentPipe } from "../common/core/time/MomentPipe";
// import { LocateService } from '@helgoland/map';
// import { MapCache } from '@helgoland/map';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';
import { DatasetByStationSelectorComponent } from './components/dataset-by-station-selector/dataset-by-station-selector.component';
import { LegendEntryComponent } from './components/legend-entry/legend-entry.component';
// import { MAT_DIALOG_DEFAULT_OPTIONS } from '@angular/material';
import { MatListModule } from '@angular/material/list';
import {MatBadgeModule} from '@angular/material/badge';

import { TimeService } from '../common/core/time/time.service';
import { InternalIdHandler } from '../common/components/services/internal-id-handler.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CoreModule } from "../common/core/core.module";

@NgModule({
  // declarations: The components, directives, and pipes that belong to this NgModule.
  declarations: [AppComponent, MapComponent, DashboardComponent, MessagesComponent, MapViewComponent, DiagramViewComponent, DatasetByStationSelectorComponent, LegendEntryComponent],
  // entryComponents: [
  //   DatasetByStationSelectorComponent
  // ],
  // imports: Other modules whose exported classes are needed by component templates declared in this NgModule.
  imports: [
    BrowserModule, HttpClientModule, AppRoutingModule, ComponentsModule, GraphjsModule, BrowserAnimationsModule, MatDialogModule, MatListModule, MatBadgeModule,
    FontAwesomeModule, CoreModule],
  providers: [
    MarkerService, PopupService, HttpService, DatasetApiService, StationService, MessageService, MapService,DatasetService, InternalIdHandler, TimeService
 
    // {
    //   provide: DatasetApiInterface,
    //   useClass: SplittedDataDatasetApiInterface,
    //   deps: [HttpXhrBackend]
    // },

    // {
    //   provide: HelgolandServicesConnector,
    //   useClass: DatasetApiV3Connector,
    //   deps: [HttpClient]
    // },

  ],
  // bootstrap: The main application view, called the root component, which hosts all other application views. 
  // Only the root NgModule should set the bootstrap property.
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor() { }
}

  // export function HttpLoaderFactory(http: HttpClient) {
  //   return new TranslateHttpLoader(http);
  // }

  // https://medium.com/@hubert.zub/using-babel-7-and-preset-typescript-to-compile-angular-6-app-448eb1880f2c