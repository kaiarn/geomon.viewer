import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DiagramViewComponent } from './views/diagram-view/diagram-view.component';

//neu
import { DashboardComponent } from './dashboard/dashboard.component';
import { MapViewComponent } from './views/map-view/map-view.component';
import { PlatformDetailComponent } from './components/platform-detail/platform-detail.component';
// import { HeroDetailComponent } from './hero-detail/hero-detail.component';

// const routes: Routes = [];

const routes: Routes = [
  { path: '', redirectTo: '/map', pathMatch: 'full' },
  
  { path: 'dashboard', component: DashboardComponent },
  { path: 'station/:id', component: PlatformDetailComponent },
  { path: 'map', component: MapViewComponent },
  { path: 'diagram', component: DiagramViewComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }