import { Component, OnInit } from '@angular/core';

import { GeomonPlatform } from './../../../shared/models/platform';
import { LayerOptions } from '../../map/map-options';
import { Marker, MapOptions, Control, icon, LatLngBoundsExpression } from 'leaflet';

import { Station } from '../../../shared/models/station';
import { DatasetByStationSelectorComponent } from './../../components/dataset-by-station-selector/dataset-by-station-selector.component';
import { MatDialog } from '@angular/material/dialog';

// optional, to adapt leaflet markers
Marker.prototype.options.icon = icon({
    iconRetinaUrl: 'assets/img/marker-icon-2x.png',
    iconUrl: 'assets/img/marker-icon.png',
    shadowUrl: 'assets/img/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    tooltipAnchor: [16, -28],
    shadowSize: [41, 41]
});

const DIALOG_MAX_WIDTH = '95%';

@Component({
    selector: 'gba-map-view',
    templateUrl: './map-view.component.html',
    styleUrls: ['./map-view.component.scss']
})
export class MapViewComponent implements OnInit {

    constructor(
        private dialog: MatDialog,
    ) {
        //
    }

    ngOnInit(): void {
        // this.serviceConnector.getServices(this.configSrvc.configuration?.defaultService.apiUrl).subscribe(services => {
        //     this.selectedService = services.find(e => e.id === this.configSrvc.configuration?.defaultService.serviceId);
        //     this.updateFilter();
        // });
    }

    // public providerUrl = 'https://geo.irceline.be/sos/api/v2/';
    public providerUrl = 'https://geomon.geologie.ac.at/52n-sos-webapp/api/';

    public fitBounds: LatLngBoundsExpression = [[9.47996951665, 46.4318173285], [16.9796667823, 49.0390742051]];
    // public zoomControlOptions: L.Control.ZoomOptions = { position: 'topleft' };
    public avoidZoomToSelection = false;
    public baseMaps: Map<string, LayerOptions> = new Map<string, LayerOptions>();
    public overlayMaps: Map<string, LayerOptions> = new Map<string, LayerOptions>();
    public layerControlOptions: Control.LayersOptions = { position: 'bottomleft' };
    public cluster = false;
    public loadingStations: boolean;
    // public stationFilter: ParameterFilter = {
    //     // phenomenon: '8'
    // };
    public statusIntervals = false;
    public mapOptions: MapOptions = {
        center: [48.208174, 16.373819],
        zoom: 3,
        zoomControl: false
    };

    public onStationSelected(station: Station) {
        // console.log('Clicked station: ' + station.properties.label);
        const dialogRef = this.dialog.open(DatasetByStationSelectorComponent, { 
            height: '400px',
            width: '600px',
            maxWidth: DIALOG_MAX_WIDTH ,
            hasBackdrop: true, //Here line to add
            // panelClass: 'custom-dialog-container'
        });
        dialogRef.componentInstance.station = station;
        dialogRef.componentInstance.url = this.providerUrl;
    }

    public onMapInitialized(newItem: string) {
        console.log(newItem);
    }

}