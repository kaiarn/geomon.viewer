import { Component, OnInit } from '@angular/core';
// import * as d3 from 'd3';

import { DatasetService } from '../../services/dataset.service';
import { AppRouterService } from '../../services/app-router.service';
// import { GeomonTimeseries, GeomonDataset } from '../../../shared/models/dataset';
import { DatasetOptions } from '../../../shared/models/options';
import { TimeService } from '../../../common/core/time/time.service';

@Component({
    selector: 'app-bar',
    templateUrl: './diagram-view.component.html',
    styleUrls: ['./diagram-view.component.css']
})
export class DiagramViewComponent implements OnInit {

    public datasetIds: string[] = [];

    public selectedIds: string[] = [];

    public datasetOptions: Map<string, DatasetOptions> = new Map();
    public datasetArray: Array<DatasetOptions>;

    public diagramLoading: boolean;
    public overviewLoading: boolean;

    // public diagramConfig: DiagramConfig = {
    //     overviewVisible: true,
    //     yaxisVisible: this.d3diagramOptions.yaxis,
    //     yaxisModifier: true,
    //     hoverstyle: this.d3diagramOptions.hoverStyle
    // };

    constructor(
        public datasetService: DatasetService<DatasetOptions>,
        public appRouter: AppRouterService,
        private timeService: TimeService) { }

    ngOnInit(): void {
        this.setDatasets();
    }

    private setDatasets() {
        this.datasetIds = this.datasetService.datasetIds;
        this.datasetOptions = this.datasetService.datasetOptions;
        this.datasetArray = Array.from(this.datasetOptions.values());
    }

    public removeAllTimeseries() {
        this.datasetService.removeAllDatasets();
    }

    public deleteTimeseries(internalId: string) {
        this.datasetService.removeDataset(internalId);
    }

    public onSelectDataset(selected: boolean, internalId: string) {
        if (selected) {
            this.selectedIds.push(internalId);
        } else {
            this.selectedIds.splice(this.selectedIds.findIndex(entry => entry === internalId), 1);
        }
    }

    public clearSelection() {
        this.selectedIds = [];
    }

    public onUpdateOptions(datasetOption: DatasetOptions, internalId: string) {
        // this.datasetService.updateDatasetOptions(datasetOption, internalId);

        // let arrIndex = this.datasetArray.findIndex(x => x.internalId === internalId);
        // this.datasetArray[arrIndex] = datasetOption;

        // this.datasetOptions = new Map <string, DatasetOptions>([]);
        // this.datasetOptions =  this.datasetService.datasetOptions;
    }

    public jumpToDate(date: Date) {
        this.datasetService.timespan = this.timeService.centerTimespan(this.datasetService.timespan, date);
    }


}
