import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';


import { GeomonTimeseries, DatasetFilter } from '../../../shared/models/dataset';
import { TimeInterval } from '../../../shared/models/timespan';
import { DatasetApiService } from '../../services/dataset-api.service';
import { InternalIdHandler, InternalDatasetId } from '../../../common/components/services/internal-id-handler.service';
import { DatasetOptions } from '../../../shared/models/options';
import { faEye, faEyeSlash, faCheck, faTimes, faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { FirstLastValue } from '../../../shared/models/dataset';
import { GeoJSON } from 'leaflet';

@Component({
    selector: 'geomon-legend-entry',
    templateUrl: './legend-entry.component.html',
    styleUrls: ['./legend-entry.component.scss']
})
export class LegendEntryComponent {
    @Input()
    public timeInterval: TimeInterval;

    @Output()
    public onSelectDate: EventEmitter<Date> = new EventEmitter();

    faEye = faEye;
    faEyeSlash = faEyeSlash;
    faChevronRight = faChevronRight;
    faCheck = faCheck;
    faTimes = faTimes;

    public firstValue: FirstLastValue;
    public lastValue: FirstLastValue;
    public hasData = true;

    public informationVisible = false;
    // public referenceValues: ReferenceValue[];


    @Input()
    public datasetOption: DatasetOptions;

    @Input()
    public highlight: boolean;

    @Output()
    public onUpdateOptions: EventEmitter<DatasetOptions> = new EventEmitter();

    // @Output()
    // public onEditOptions: EventEmitter<DatasetOptions> = new EventEmitter();

    @Output()
    public onShowGeometry: EventEmitter<GeoJSON.GeoJsonObject> = new EventEmitter();

    public dataset: GeomonTimeseries;
    public platformLabel: string;
    public phenomenonLabel: string;
    public procedureLabel: string;
    public categoryLabel: string;
    public uom: string;
    public error: any;



    @Input()
    public datasetId: string;

    @Input()
    public selected: boolean;

    @Output()
    public onDeleteDataset: EventEmitter<boolean> = new EventEmitter();

    @Output()
    public onSelectDataset: EventEmitter<boolean> = new EventEmitter();

    public loading: boolean;
    protected internalId: InternalDatasetId;
    // private langChangeSubscription: Subscription;

    constructor(
        private internalIdHandler: InternalIdHandler,
        private datasetApiService: DatasetApiService) { }

    public ngOnInit(): void {
        if (this.datasetId) {
            this.internalId = this.internalIdHandler.resolveInternalId(this.datasetId);
            this.loadDataset();
        }

    }

    public jumpToFirstTimeStamp() {
        this.onSelectDate.emit(new Date(this.dataset.firstValue.timestamp));
    }

    public jumpToLastTimeStamp() {
        this.onSelectDate.emit(new Date(this.dataset.lastValue.timestamp));
      }

    protected loadDataset(locale?: string): void {
        const params: DatasetFilter = {};
        if (locale) { params.locale = locale; }
        this.loading = true;
        // this.servicesConnector.getDataset(this.internalId, { ...params, type: DatasetType.Timeseries })
        //   .subscribe(
        //     dataset => this.setDataset(dataset),
        //     error => this.handleError(error)
        //   );
        this.datasetApiService.getDataset(this.internalId.id, this.internalId.url, { type: 'timeseries' }).subscribe({
            next: (res: GeomonTimeseries) => this.setDataset(res),
            error: (err: any) => this.handleError(err),
            complete: () => console.log('HTTP request completed.')
            // error => this.errorHandler.handleDatasetLoadError(error)
        });
    }

    protected handleError(error: any) {
        this.loading = false;
        this.error = error;
    }

    public removeDataset() {
        this.onDeleteDataset.emit(true);
    }

    protected setDataset(timeseries: GeomonTimeseries) {
        this.dataset = timeseries;
        this.setParameters();
        this.loading = false;
    }

    private setParameters() {
        this.platformLabel = this.dataset.parameters.platform.label;
        this.phenomenonLabel = this.dataset.parameters.phenomenon.label;
        this.procedureLabel = this.dataset.parameters.procedure.label;
        this.categoryLabel = this.dataset.parameters.category.label;
        this.uom = this.dataset.uom;

        this.firstValue = this.dataset.firstValue;
        this.lastValue = this.dataset.lastValue;
        // this.checkDataInTimespan();

    }

    // private checkDataInTimespan() {
    //     if (this.timeInterval && this.dataset && this.dataset.firstValue && this.dataset.lastValue) {
    //         this.hasData = this.timeService.overlaps(
    //             this.timeInterval,
    //             this.dataset.firstValue.timestamp,
    //             this.dataset.lastValue.timestamp
    //         );
    //     }
    // }

    public toggleSelection() {
        this.selected = !this.selected;
        this.onSelectDataset.emit(this.selected);
    }

    public toggleVisibility() {
        this.datasetOption.visible = !this.datasetOption.visible;
        this.onUpdateOptions.emit(this.datasetOption);
    }

    public editDatasetOptions() {
        // this.onEditOptions.emit(this.datasetOptions);
    }

    public showGeometry() {
        this.onShowGeometry.emit(this.dataset.platform.geometry);
    }

}