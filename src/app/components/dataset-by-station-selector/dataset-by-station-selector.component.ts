import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { GeomonPlatform } from './../../../shared/models/platform';
import { DatasetApiService } from './../../services/dataset-api.service';
// https://github.com/52North/helgoland-toolbox/blob/fe6af1b9df0e5d78eeec236e4690aeb7dc92119b/libs/selector/src/lib/dataset-by-station-selector/dataset-by-station-selector.component.ts#L20
import { GeomonTimeseries, GeomonDataset } from './../../../shared/models/dataset';
import { Station } from './../../../shared/models/station';

import { MatDialogRef } from '@angular/material/dialog';
import { MatSelectionListChange } from '@angular/material/list';

import { DatasetService } from '../../services/dataset.service';
import { AppRouterService } from './../../services/app-router.service';
import { DatasetOptions } from './../../../shared/models/options';

// https://material.angular.io/components/dialog/overview
// https://blog.angular-university.io/angular-material-dialog/
// https://github.com/angular-university/angular-material-course/blob/3-dialog-finished/src/app/course-dialog/course-dialog.component.html

export class SelectableDataset extends GeomonTimeseries {
    public selected: boolean;
}

@Component({
    selector: 'gba-dataset-by-station-selector',
    templateUrl: './dataset-by-station-selector.component.html',
    styleUrls: ['./dataset-by-station-selector.component.scss']
})
export class DatasetByStationSelectorComponent implements OnInit {

    @Input()
    public station: Station;//GeomonPlatform;

    public platform: GeomonPlatform;

    @Input()
    public url: string;

    @Input()
    public defaultSelected = false;

    @Input()
    public phenomenonId: string;

    @Output()
    public onSelectionChanged: EventEmitter<SelectableDataset[]> = new EventEmitter<SelectableDataset[]>();

    public phenomenonMatchedList: SelectableDataset[] = [];
    public othersList: SelectableDataset[] = [];

    public counter: number;

    constructor(
        protected datasetApiService: DatasetApiService,
        private dialogRef: MatDialogRef<DatasetByStationSelectorComponent>,
        public datasetService : DatasetService<DatasetOptions>,
        public appRouter: AppRouterService,
    ) { }

    public ngOnInit() {
        this.datasetApiService.getPlatform(this.station.id, 'https://geomon.geologie.ac.at/52n-sos-webapp/api/')
            .subscribe((platform) => {
                this.platform = platform;
                this.counter = 0;

                this.platform.datasetIds.forEach(id => {
                    this.counter++;
                    this.datasetApiService.getDataset( id, this.url, { type: 'timeseries' })
                        .subscribe((result) => {
                            this.prepareResult(result as SelectableDataset, this.defaultSelected);
                            this.counter--;
                        }, (error) => {
                            this.counter--;
                        });
                });


            });
    }

    public toggle(timeseriesDataset: SelectableDataset) {
        timeseriesDataset.selected = !timeseriesDataset.selected;
        this.updateSelection();     
    }

    protected prepareResult(result: SelectableDataset, selection: boolean) {
        if (this.datasetService.hasDataset(result.internalId)) {
            selection = true;
          }
        result.selected = selection;
        // if (this.phenomenonId) {
        //     if (result.parameters.phenomenon.id === this.phenomenonId) {
        //         this.phenomenonMatchedList.push(result);
        //     } else {
        //         this.othersList.push(result);
        //     }
        // } 
        // else {
            this.phenomenonMatchedList.push(result);
        // }
        this.updateSelection();
    }

    private updateSelection() {
        const selection = this.phenomenonMatchedList.filter((entry) => entry.selected);
        this.onSelectionChanged.emit(selection);
    }

    public close() {
        this.dialogRef.close();
    }

    public adjustSelection(change: MatSelectionListChange) {
       
        const id = (change.option.value as SelectableDataset).internalId;
        if (change.option.selected) {
          this.datasetService.addDataset(id, change.option.value as DatasetOptions);
        } else {
          this.datasetService.removeDataset(id);
        }
      }

}
