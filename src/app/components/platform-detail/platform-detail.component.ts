import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GeomonPlatform } from '../../../shared/models/platform';
import { DatasetApiService } from '../../services/dataset-api.service';

// import { Hero } from '../hero';
// import { HeroService } from '../hero.service';

@Component({
  selector: 'geomon-platform-detail',
  templateUrl: './platform-detail.component.html',
  styleUrls: ['./platform-detail.component.scss']
})
export class PlatformDetailComponent implements OnInit {

  public platform: GeomonPlatform | undefined;
  public loading: boolean;
  public error: any;
  title: string = 'AppComponent';

  constructor(
    private route: ActivatedRoute,
    private datasetService: DatasetApiService,
    private location: Location
  ) {
    this.getPlatform();
  }

  ngOnInit(): void {
    // this.getHero();
  }

  getPlatform(): void {
    const id = String(this.route.snapshot.paramMap.get('id'));
    this.datasetService.getPlatform(id, 'https://geomon.geologie.ac.at/52n-sos-webapp/api/')
		.subscribe({
      next: (platform: GeomonPlatform) => {
        this.platform = platform;
        this.title = "neuer Titel im Event";
      },
      error: (err: any) => this.handleError(err)
		});   
    
   
  }

  protected handleError(error: any) {
    this.loading = false;
    this.error = error;
}

  goBack(): void {
    this.location.back();
  }

}
