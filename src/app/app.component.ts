import { Component, VERSION, AfterViewInit } from "@angular/core";
import '../styles.scss';
// import '../../node_modules/leaflet/dist/leaflet.css';

// Marker.prototype.options.icon = icon({
//     iconRetinaUrl: 'assets/img/marker-icon-2x.png',
//     iconUrl: 'assets/img/marker-icon.png',
//     shadowUrl: 'assets/img/marker-shadow.png',
//     iconSize: [25, 41],
//     iconAnchor: [12, 41],
//     popupAnchor: [1, -34],
//     tooltipAnchor: [16, -28],
//     shadowSize: [41, 41]
// });


@Component({
    selector: "app-component",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.css"]
})
export class AppComponent implements AfterViewInit {

    constructor() { }

    ngAfterViewInit(): void {
        const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
        // Check if there are any navbar burgers
        if ($navbarBurgers.length > 0) {
            // Add a click event on each of them
            $navbarBurgers.forEach(el => {
                el.addEventListener('click', () => {

                    // Get the target from the "data-target" attribute
                    const target = el.dataset.target;
                    const $target = document.getElementById(target);

                    // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                    el.classList.toggle('is-active');
                    $target.classList.toggle('is-active');

                });
            });
        }
    }

    public name = 'Angular test ' + VERSION.major;
    x: number = 123;

    // // public providerUrl = 'https://geo.irceline.be/sos/api/v2/';
    // public providerUrl = 'https://geomon.geologie.ac.at/52n-sos-webapp/api/';

    // public fitBounds: LatLngBoundsExpression = [[9.47996951665, 46.4318173285], [16.9796667823, 49.0390742051]];
    // // public zoomControlOptions: L.Control.ZoomOptions = { position: 'topleft' };
    // public avoidZoomToSelection = false;
    // public baseMaps: Map<string, LayerOptions> = new Map<string, LayerOptions>();
    // public overlayMaps: Map<string, LayerOptions> = new Map<string, LayerOptions>();
    // public layerControlOptions: Control.LayersOptions = { position: 'bottomleft' };
    // public cluster = false;
    // public loadingStations: boolean;
    // public stationFilter: ParameterFilter = {
    //     // phenomenon: '8'
    // };
    // public statusIntervals = false;
    // public mapOptions: MapOptions = { dragging: true, zoomControl: false };

    // public onStationSelected(station: Station) {
    //     console.log('Clicked station: ' + station.properties.label);
    // }

    // public onMapInitialized(newItem: string) {
    //     console.log(newItem);
    // }
}
