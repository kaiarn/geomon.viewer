import { Component, OnInit } from '@angular/core';
// import { StationService } from '../services/station.service';
import { DatasetApiService } from '../services/dataset-api.service';
import { Station } from '../../shared/models/station';
import { GeomonPlatform } from '../../shared/models/platform';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	public stations: GeomonPlatform[] = [];

	constructor(private datasetApiService: DatasetApiService) { }

	ngOnInit() {
		this.getStations();
	}

	getStations(): void {
		// this.stationService.getStations()
		// 	.subscribe(stations => this.stations = stations);
		this.datasetApiService.getPlatforms('https://geomon.geologie.ac.at/52n-sos-webapp/api/')
		.subscribe((stations) => {
			this.stations = stations;
		});
	}

}
