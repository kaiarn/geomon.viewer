// https://github.com/52North/helgoland-toolbox/blob/c2c2eda20353c469a7aa4a6a118a810723af6622/libs/map/src/lib/selector/station-map-selector/station-map-selector.component.ts
import { Component, AfterViewInit, OnChanges, SimpleChanges, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { Control, FeatureGroup, geoJSON, circleMarker, FitBoundsOptions, LatLngBoundsExpression } from 'leaflet';
import { MarkerService } from '../services/marker.service';
import { DatasetApiService } from '../services/dataset-api.service';
import { BaseMapComponent } from './base-map.component';
import { PopupService } from '../services/popup.service';
import { MapService } from '../../common/components/services/map.service';
import { ZoomControlComponent } from '../../common/components/zoom-control/zoom.component';

@Component({
	selector: 'app-map',
	templateUrl: './map.component.html',
	styleUrls: ['./map.component.css'],
})
// https://52north.github.io/helgoland-toolbox/classes/CachedMapComponent.html#source
export class MapComponent
	extends BaseMapComponent
	implements OnChanges, AfterViewInit {

	@ViewChild(ZoomControlComponent) zoom:ZoomControlComponent;

	
	/**
	* @input The serviceUrl, where the selection should be loaded.
	*/
	// @Input()
	// public serviceUrl: string;

	@Output()
	public onSelected: EventEmitter<any> = new EventEmitter<any>();

	@Output()
	public onContentLoadingEvent: EventEmitter<boolean> = new EventEmitter();

	/**
	* @input Additional configuration for the marker zooming
	*/
	@Input()
	public fitBoundsMarkerOptions: FitBoundsOptions;

	protected oldBaseLayer: Control.LayersObject = {};
	// protected map: Map;
	// protected zoomControl: Control.Zoom;
	protected markerFeatureGroup: FeatureGroup;

	// constructor() { }
	constructor(
		protected mapService: MapService,
		protected markerService: MarkerService,
		private popupService: PopupService,
		protected datasetApiService: DatasetApiService) {
			super(mapService);
			this.markerFeatureGroup = new FeatureGroup();
			
	}

	ngAfterViewInit(): void {
		this.initMap();
		this.map.on("zoomend zoomlevelschange", this.zoom._updateDisabled, this.zoom);
		// this.markerService.makeCapitalMarkers(this.map);
		// this.markerService.makeCapitalCircleMarkers(this.map);
		this.datasetApiService.getStations('https://geomon.geologie.ac.at/52n-sos-webapp/api/')
			.subscribe((res) => {
				// this.markerFeatureGroup = new FeatureGroup();
				if (res instanceof Array && res.length > 0) {
					res.forEach((station) => {
						//const marker = this.createDefaultGeometry(entry);
						// const marker = geoJSON(entry.geometry);
						const marker = geoJSON(station.geometry, {
							pointToLayer: function (feature, latlng) {
								return circleMarker(latlng);
							}
						})
						// if (marker) { this.markerFeatureGroup.addLayer(marker); }
						marker && this.markerFeatureGroup.addLayer(marker);
						if (marker) {
							marker.on('click', () => {
								this.onSelected.emit(station);
							});
							// marker.bindPopup(this.popupService.makeCapitalPopup(station.properties));
						}
					});
					this.markerFeatureGroup.addTo(this.map);
					this.zoomToMarkerBounds(this.markerFeatureGroup.getBounds());
				} else {
					// this.onNoResultsFound.emit(true);
				}
				this.map.invalidateSize();

				this.onContentLoadingEvent.emit(false);
			});
		// this.createStationGeometries();
	}
	
	public ngOnChanges(changes: SimpleChanges) {
		super.ngOnChanges(changes);
		if (this.map && changes.statusIntervals) { this.drawGeometries(); }
	}

	protected drawGeometries() {
		this.onContentLoadingEvent.emit(true);
		// if (this.map && this.markerFeatureGroup) { this.map.removeLayer(this.markerFeatureGroup); }
		// if (this.statusIntervals && this.filter && this.filter.phenomenon) {
		//     this.createValuedMarkers();
		// } else {
		//     this.createStationGeometries();
		// }
	}

	 /**
     * Zooms to the given bounds
     *
     * @protected
     * @param bounds where to zoom
     */
	  protected zoomToMarkerBounds(bounds: LatLngBoundsExpression) {       
        this.map.fitBounds(bounds, this.fitBoundsMarkerOptions || {});        
    }

}
