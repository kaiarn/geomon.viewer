import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MomentPipe } from './time/moment-pipe';
import { DatasetApiService } from '../../app/services/dataset-api.service';


@NgModule({
  imports: [    
    CommonModule
  ],
  declarations: [
    MomentPipe
  ],  
  exports: [ 
    MomentPipe
  ],
  providers: [      
  ]
})
export class CoreModule { }