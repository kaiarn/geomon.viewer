import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({ name: 'dateFormat' })
export class MomentPipe implements PipeTransform {
    
    transform(date: Date | moment.Moment | number | string, dateFormat: string): any {
        if (typeof (date) === 'number') { date = moment(date); }
        if (typeof (date) === 'string') { date = moment(date); }
        if (date instanceof Date) { date = moment(date); }

        if (!dateFormat) { dateFormat = 'L LT z'; }
        return moment(date).format(dateFormat);
    }
}