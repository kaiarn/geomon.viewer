import { Injectable } from '@angular/core';

import { Timespan, TimeInterval, BufferedTime } from '../../../shared/models/timespan';
import * as moment from 'moment';
// import 'moment-duration-format';

@Injectable()
export class TimeService {

    constructor() { }

    public centerTimespan(timespan: Timespan, date: Date): Timespan {
        const halfduration = this.getDuration(timespan).asMilliseconds() / 2;
        const from = moment(date).subtract(halfduration).unix() * 1000;
        const to = moment(date).add(halfduration).unix() * 1000;
        return new Timespan(from, to);
    }

    public getBufferedTimespan(timespan: Timespan, factor: number, maxBufferInMs?: number): Timespan {
        const durationMillis = this.getDuration(timespan).asMilliseconds();
        let buffer = durationMillis * factor;
        if (maxBufferInMs && buffer > maxBufferInMs) {
            buffer = maxBufferInMs;
        }
        const from = timespan.from - buffer;
        const to = timespan.to + buffer;
        return new Timespan(from, to);
    }

    
    private getDuration(timespan: Timespan): moment.Duration {
        const from = moment(timespan.from);
        const to = moment(timespan.to);
        return moment.duration(to.diff(from));
    }

    public createByDurationWithEnd(d: moment.Duration, end: number | Date, endOf?: moment.unitOfTime.StartOf): Timespan {
        const mEnd = moment(end);
        if (endOf) {
            mEnd.endOf(endOf);
        }
        const mStart = moment(mEnd).subtract(d);
        return new Timespan(mStart.toDate(), mEnd.toDate());
    }

    public createTimespanOfInterval(timeInterval: TimeInterval): Timespan {
        
        if(timeInterval instanceof BufferedTime) {
            const d = moment.duration(timeInterval.bufferInterval / 2);
            const from = moment(timeInterval.timestamp).subtract(d).unix() * 1000;
            const to = moment(timeInterval.timestamp).add(d).unix() * 1000;
            return new Timespan(from, to);
        }  else {
                return timeInterval as Timespan;
        }
    }


}