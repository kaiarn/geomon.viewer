import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { GeomonTimeseriesChartComponent } from './geomon-timeseries-chart/geomon-timeseries-chart.component';
// import { ZoomControlComponent } from './zoom-control/zoom.component';

import { DatasetApiService } from '../../app/services/dataset-api.service';

import { TimeService } from '../core/time/time.service';


@NgModule({
  imports: [    
    CommonModule
  ],
  declarations: [ 
    GeomonTimeseriesChartComponent,  
  ],  
  exports: [ 
    GeomonTimeseriesChartComponent
   
  ],
  providers: [
    DatasetApiService , TimeService
  ]
})
export class GraphjsModule { }