import { Injectable } from '@angular/core';
import { Map, Marker } from 'leaflet';
import { MapService } from './map.service';

const LOCATION_FOUND_EVENT = 'locationfound';
const LOCATION_ERROR = 'locationerror';
const LOCATED_MARKER_ID = 'located';

@Injectable()
export class LocateService {

    constructor(
        protected mapService: MapService
    ) { }

    public startLocate(id: string) {
        const map = this.mapService.getMap(id);
        map.on(LOCATION_FOUND_EVENT, (evt: L.LocationEvent) => {
            this.removeMarker(map);
            const marker = new Marker(evt.latlng).addTo(map);
            marker.options.title = LOCATED_MARKER_ID;
        });
        map.on(LOCATION_ERROR, (error) => {
            console.error(error);
        });
        map.locate({
            watch: true,
            setView: true,
            timeout: 30000
        });
    }

    public stopLocate(id: string) {
        const map = this.mapService.getMap(id);
        map.stopLocate();
        map.off(LOCATION_FOUND_EVENT);
        this.removeMarker(map);
    }

    private removeMarker(map: Map) {
        map.eachLayer((entry) => {
            if (entry instanceof Marker && entry.options.title === LOCATED_MARKER_ID) {
                map.removeLayer(entry);
            }
        });
    }

}