import { Injectable } from '@angular/core';
import * as L from 'leaflet';

@Injectable()
export class MapService {

    private mapService: Map<string, any> = new Map<string, any>();

    public getMap(id: string): L.Map {
        return this.mapService.get(id);
    }

    public setMap(id: string, map: L.Map) {
        this.mapService.set(id, map);
    }

    public hasMap(id: string): boolean {
        return this.mapService.has(id);
    }

    public deleteMap(id: string): boolean {
        return this.mapService.delete(id);
    }

}