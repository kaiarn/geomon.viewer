import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';


import { LocateButtonComponent } from './locate-button/locate-button.component';
import { ZoomControlComponent } from './zoom-control/zoom.component';
import { MapService } from './services/map.service';
import { LocateService } from './services/locate.service';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

@NgModule({
  imports: [    
    CommonModule, FontAwesomeModule
  ],
  declarations: [ 
    LocateButtonComponent, ZoomControlComponent  
  ],  
  exports: [ 
    LocateButtonComponent, ZoomControlComponent
   
  ],
  providers: [
    // LocateService,
    MapService, LocateService
  ]
})
export class ComponentsModule { }