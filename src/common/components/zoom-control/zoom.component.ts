import { Component, Input, ElementRef, ViewChild  } from '@angular/core';
import { MapService } from '../services/map.service';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'gba-zoom-control',
    templateUrl: './zoom.component.html',
    styleUrls: ['./zoom.component.scss']
})
export class ZoomControlComponent {

    faPlus = faPlus;
    faMinus = faMinus;

    /**
     * Connect map id.
     */
    @Input() public mapId: string;

    @ViewChild('inputPlus') private _inputPlus:ElementRef; 
    @ViewChild('inputMinus') private _inpuMinus:ElementRef;

    constructor(
        protected mapService: MapService
    ) {
    }

    public zoomIn() {
        let map = this.mapService.getMap(this.mapId);
        map.zoomIn();
    }

    public zoomOut() {
        let map = this.mapService.getMap(this.mapId);
        map.zoomOut();
    }

    _updateDisabled() {
		let map = this.mapService.getMap(this.mapId);
		// let className = 'leaflet-disabled';

        // this._inputPlus.nativeElement.classList.remove(className);
        this._inputPlus.nativeElement.disabled = false;
		this._inputPlus.nativeElement.setAttribute('aria-disabled', 'false');

        // this._inpuMinus.nativeElement.classList.remove(className);
        this._inpuMinus.nativeElement.disabled = false;
		this._inpuMinus.nativeElement.setAttribute('aria-disabled', 'false');

		if (map.getZoom() === map.getMinZoom()) {
            // this._inpuMinus.nativeElement.classList.add(className);
            this._inpuMinus.nativeElement.disabled = true;
            this._inpuMinus.nativeElement.setAttribute('aria-disabled', 'true');           
		}
		if (map.getZoom() === map.getMaxZoom()) {
            // this._inputPlus.nativeElement.classList.add(className);
            this._inputPlus.nativeElement.disabled = true;
			this._inputPlus.nativeElement.setAttribute('aria-disabled', 'true');
		}
	}
}