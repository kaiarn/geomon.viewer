
import { Component, Input } from '@angular/core';
import { MapService } from '../services/map.service';
import { LocateService } from '../services/locate.service';
import { faSearchLocation, faMapPin } from '@fortawesome/free-solid-svg-icons';

@Component({
	selector: 'gba-locate-button',
	templateUrl: './locate-button.component.html',
	styleUrls: ['./locate-button.component.scss']
})
export class LocateButtonComponent  {

    faSearchLocation = faSearchLocation;

    @Input()
    public mapId: string;

    public isToggled = false;

    constructor(
        protected locateService: LocateService,
        protected mapCache: MapService
    ) {
        // super(mapCache);
    }

    public locateUser() {
        this.isToggled = !this.isToggled;
        if (this.isToggled) {
            this.locateService.startLocate(this.mapId);
			// alert('toggled');
        } else {
            this.locateService.stopLocate(this.mapId);
			// alert('not toggled');
        }
    }
}