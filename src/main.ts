// import 'zone.js';
import './polyfills';

// import 'core-js/es/reflect';
// require('zone.js/dist/zone');

import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";


import { AppModule } from './app/app.module';
import { environment } from './environments/environment.prod';

if (environment.production) {
  enableProdMode();
}

// import { Component } from "@angular/core";
// @Component({
//   selector: "app-component",
//   template: "<div>AppComponent works!</div>",
// })
// export class AppComponent {
//   constructor() {}
// }

// import { NgModule } from "@angular/core";
// import { BrowserModule } from "@angular/platform-browser";
// @NgModule({
//   declarations: [AppComponent],
//   imports: [BrowserModule],
//   bootstrap: [AppComponent],
// })
// export class AppModule {
//   constructor() {}
// }

// platformBrowserDynamic().bootstrapModule(AppModule);
platformBrowserDynamic().bootstrapModule(AppModule)
  .catch(err => console.error(err));