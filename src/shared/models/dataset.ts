import { GeomonPlatform } from "./platform";
import { InternalIdHandler } from '../../common/components/services/internal-id-handler.service';

const INTERNAL_ID_SEPERATOR = '_';

export class Dataset  {
    public id: string;
    public label: string;
    public url: string;
    public uom: string;
    public internalId: string;
    public firstValue: FirstLastValue;
    public lastValue: FirstLastValue;
    public referenceValues: ReferenceValue[];
    public datasetType: any;
    public platformType: any;
    public seriesParameters?: any;
    public renderingHints: RenderingHints;
    public parameters: any;
}

export interface IDataset extends Parameter {
    url: string;
    uom: string;
    internalId: string;
    firstValue: FirstLastValue;
    lastValue: FirstLastValue;
    referenceValues: ReferenceValue[];
    parameters: ParameterConstellation;
    renderingHints: RenderingHints;
}

export class GeomonDataset {
    public internalId: string;

    constructor(
        public id: string,
        public url: string,
        public label: string
    ) {
        this.internalId = new InternalIdHandler().createInternalId(url, id);
    }
    // public get internalId(): string {
    //     return this.url + INTERNAL_ID_SEPERATOR + this.id;
    // }
}

export class GeomonTimeseries extends GeomonDataset {

    constructor(
        public id: string,
        public url: string,
        public label: string,
        public uom: string,
        public platform: GeomonPlatform,
        public firstValue: FirstLastValue,
        public lastValue: FirstLastValue,
        public referenceValues: ReferenceValue[],
        public renderingHints: RenderingHints,
        public parameters: ParameterConstellation,
    ) {
        super(id, url, label);
    }   
}

export class ParameterConstellation {
    public service: Parameter;
    public offering: Parameter;
    public feature: Parameter; //Feature;
    public procedure: Parameter;
    public phenomenon: Parameter;
    public category: Parameter;
    public platform: Parameter;
}
interface Parameter  {
    id: string;
    label: string;
}

export class FirstLastValue {
    public timestamp: number;
    public value: number;
}

export class ReferenceValue {
    public referenceValueId: string;
    public label: string;
    public lastValue: FirstLastValue;
    public color?: string;
    public visible?: boolean;
}

export interface RenderingHints {
    chartType: string;
    properties: {
        color: string;
    };
}

export interface DataConst extends GeomonTimeseries {
    data?: Data<TimeValueTuple>;
}


export interface DatasetFilter {
    phenomenon?: string;
    category?: string;
    procedure?: string;
    feature?: string;
    offering?: string;
    service?: string;
    expanded?: boolean;
    locale?: string;
    type?: DatasetType;
}

export enum DatasetType {
    Timeseries = 'timeseries',
    Trajectory = 'trajectory',
    Profile = 'profile'
}

// export class TimeseriesData {
//     public id: string;
//     public url: string;
//     public data: FirstLastValue[];
// }

export interface IDataEntry { }

export interface Data<T extends IDataEntry> {
    values: T[];
    referenceValues: ReferenceValues<T>;
    valueBeforeTimespan?: T;
    valueAfterTimespan?: T;
}

export class ReferenceValues<T extends IDataEntry> {
    [key: string]: {
        values: T[];
        valueBeforeTimespan?: T;
        valueAfterTimespan?: T;
    };
}




export type TimeValueTuple = [number, number];
export interface GeomonData { }
export class GeomonTimeseriesData implements GeomonData {

    referenceValues: ReferenceValues<TimeValueTuple> = {};
    valueBeforeTimespan: [number, number]; // TimeValueTuple;
    valueAfterTimespan: [number, number]; //TimeValueTuple;

    constructor(
        public values: Array<[number, number]>,
    ) { }
}

export interface HelgolandDataFilter {
    expanded?: boolean;
    generalize?: boolean;
}