import { GeoJSON } from 'leaflet';

export interface Station {
    id: string;
    label: string;
    geometry: GeoJSON.GeometryObject;
    properties: StationProperties;
}

export interface StationProperties {
    id: string;
    label: string;
    datasets: Dataset;
}

export interface Dataset 
{
    id: string;
    label: string;
    href: string,
    domainId: string;
}
// export class TimeseriesCollection {
//     [key: string]: ParameterConstellation;
// }

// interface Timeseries {
//     id: string;
//     label: string;
//     url: string;
//     uom: string;
//     internalId: string;
//     // firstValue;
//     // lastValue;
//     // referenceValues;
//     station: Station;
//     // parameters;
//     // statusIntervals?;
//     // hasData = false;
//     // public renderingHints;
// }