import { DatasetOptions } from "./options";
import { Duration, unitOfTime } from 'moment';

export interface DataEntry {
    timestamp: number;
    value: number;
    xDiagCoord?: number;
    yDiagCoord?: number;
}

export interface InternalDataEntry {
    internalId: string;
    datasetIndex?: number;
    // hoverId: string;
    data: number[];
    selected?: boolean;
    options: DatasetOptions;
    bar?: {
        startOf: unitOfTime.StartOf;
        period: Duration;
    };
    axisOptions: {
        uom: string;
        label?: string;
        // zeroBased?: boolean;
        // // yAxisRange?: MinMaxRange;
        // autoRangeSelection?: boolean;
        // separateYAxis?: boolean;
        parameters?: {
            feature?: { id: string, label: string };
            phenomenon?: { id: string, label: string };
            offering?: { id: string, label: string };
        };
    };
    referenceValueData: {
        id: string;
        color: string;
        data: DataEntry[];
    }[];
    visible: boolean;
}