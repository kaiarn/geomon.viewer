import { FirstLastValue } from './dataset';

export class DatasetOptions {

    /**
     * internal dataset id
     */
    public internalId: string;

    /**
     * type to display the data
     * default is 'line'
     */
    public type: 'line' | 'bar' = 'line';

    /**
     * color of the dataset
     */
    public color: string;

    /**
     * show or hide in the graph
     */
    public visible: boolean = true;

     
    /**
     * list of visible reference values
     */
    public showReferenceValues: ReferenceValueOption[] = [];

    /**
     * radius of graphpoint
     * default is 0
     */
    public pointRadius: number = 0;

    /**
     * the start of, where to start with the bar chart
     * See also: https://momentjs.com/docs/#/manipulating/start-of/
     * default is 'hour'
     */
    public barStartOf: string = 'hour';


    /**
     * width of graphline
     */
    public lineWidth: number = 1;

    /**
     * dasharray to structure the line or bar chart border
     * See also here: https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray
     */
    public lineDashArray: number | number[];

    /**
     * color of the point border
     */
    public pointBorderColor: string;

    /**
     * width of the point border
     */
    public pointBorderWidth: number = 0;

    /**
     * min and max range of y axis
     */
    public yAxisRange?: MinMaxRange;

    public firstValue: FirstLastValue;
    public lastValue: FirstLastValue;

    constructor(
        internalId: string,
        color: string
    ) {
        this.internalId = internalId;
        this.color = color;
        this.visible = true;
    }
}

export interface ReferenceValueOption {
    id: string;
    color: string;
}

/**
 * numbered range with a min and a max value
 *
 * @export
 */
export interface MinMaxRange {
    min?: number;
    max?: number;
}
