export interface Phenomenon {
    id: string;
    label: string;
}