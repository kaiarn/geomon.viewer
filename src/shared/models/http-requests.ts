export interface HttpRequestOptions {
    forceUpdate?: boolean;
    basicAuthToken?: string;
    expirationAtMs?: number;
}