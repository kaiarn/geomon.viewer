const { merge } = require('webpack-merge');
const common = require('./webpack.common.js');
const path = require('path');

module.exports = merge(common, {
    mode: 'development',
    devtool: 'inline-source-map',
    devServer: {
        contentBase: path.resolve(__dirname),
        publicPath: "/", //should provide the path of the served js , img , etc...
        writeToDisk: true,
        compress: false,
        port: 8080,
        open: false,
        stats: 'errors-only',
    },
});